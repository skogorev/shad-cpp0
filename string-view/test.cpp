#include <catch.hpp>
#include <util.h>
#include <string_view.h>

#include <string>

TEST_CASE("Constructors") {
    {
        StringView s("abacaba");
        REQUIRE('c' == s[3]);
        REQUIRE(7u == s.Size());
    }
    {
        StringView s("caba", 3);
        REQUIRE('c' == s[0]);
        REQUIRE(3u == s.Size());
    }
    {
        std::string a("abacaba");
        StringView s(a);
        REQUIRE('c' == s[3]);
        REQUIRE(7u == s.Size());
    }
    {
        std::string a("abacaba");
        StringView s(a, 3);
        REQUIRE('c' == s[0]);
        REQUIRE(4u == s.Size());
    }
    {
        std::string a("abacaba");
        StringView s(a, 3, 3);
        REQUIRE('a' == s[1]);
        REQUIRE(3u == s.Size());
    }
}

TEST_CASE("Constness") {
    std::string a("abacaba");
    const StringView kS(a, 3, 3);
    REQUIRE('b' == kS[2]);
    REQUIRE(3u == kS.Size());
}

TEST_CASE("Big") {
    RandomGenerator rnd(38545678);
    const int kCount = 1e5;
    auto a = rnd.GenString(kCount);
    StringView s(a, 1);

    for (int i = 0; i < kCount; ++i) {
        int ind = rnd.GenInt(1, kCount - 1);
        a[ind] = rnd.GenInt('a', 'z');
        REQUIRE(a[ind] == s[ind - 1]);
    }

    REQUIRE(kCount - 1 == static_cast<int>(s.Size()));
}
